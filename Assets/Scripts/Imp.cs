using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Imp : Enemy
{
    [SerializeField] private float timeMoving;

    [Header("Combat")]
    [SerializeField] private float timePausedForAttack;
    [SerializeField] private int damage;
    [SerializeField] private float fireballMoveSpeed;
    [SerializeField] private GameObject fireball;

    private Vector2 movement;

    private void Start()
    {
        StartCoroutine(StartMovement());
    }

    private void Update()
    {
        LookTowardsThePlayer();
        rb.velocity = movement;
    }

    private IEnumerator StartMovement()
    {
        //Chooses a random direction to move
        Vector2 newMovement = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        newMovement = newMovement.normalized * moveSpeed;
        rb.velocity = newMovement;
        movement = rb.velocity;

        yield return new WaitForSeconds(timeMoving + Random.Range(-timeMoving * 0.2f, -timeMoving * 0.2f));

        //Switches to attacking
        StartCoroutine(Attack());
    }

    private IEnumerator Attack()
    {
        //Pauses
        rb.velocity = Vector2.zero;
        movement = rb.velocity;

        //Wait
        yield return new WaitForSeconds(timePausedForAttack / 2);

        //Shoots at the player
        //Gets the direction to the player
        Vector2 projectileDirection = Player.instance.transform.position - transform.position;
        projectileDirection = projectileDirection.normalized;

        //Creates a magic missile
        GameObject newFireball = Instantiate(fireball, transform.position, Quaternion.identity);
        newFireball.GetComponent<Projectile>().Initialize(Projectile.TargetType.Player, projectileDirection, damage, fireballMoveSpeed);

        //Wait
        yield return new WaitForSeconds(timePausedForAttack / 2);

        //Return to move
        StartCoroutine(StartMovement());
    }
}
