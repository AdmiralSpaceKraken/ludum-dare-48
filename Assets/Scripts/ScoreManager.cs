using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScoreManager
{
    private static int score;
    public static int Score { get { return score; } }

    /// <summary>
    /// Increases the score by the given amount
    /// </summary>
    /// <param name="scoreIncrease">The amount to increase the score by</param>
    public static void AddScore(int scoreIncrease)
    {
        score += scoreIncrease;
    }

    /// <summary>
    /// Resets the score to 0
    /// </summary>
    public static void ResetScore()
    {
        score = 0;
    }
}
