using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour
{
    public static UserInterface instance;

    [SerializeField] private Image healthBar;
    [SerializeField] private Image dashBar;
    [SerializeField] private Text portalTimer;
    [SerializeField] private Text scoreDisplay;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        scoreDisplay.text = ScoreManager.Score.ToString();
    }

    /// <summary>
    /// Updates how long the health bar should be
    /// </summary>
    /// <param name="healthPercent">The percentage of the health bar to be full (0 - 1.0f)</param>
    public void UpdateHealthBar(float healthPercent)
    {
        healthBar.fillAmount = healthPercent;

        //Changes healthbar color based on percent left
        if(healthPercent > 0.8f)
        {
            healthBar.color = new Color(0, 1, 0);
        }
        else if(healthPercent > 0.5f)
        {
            healthBar.color = new Color(1, 1, 0);
        }
        else if (healthPercent > 0.3f)
        {
            healthBar.color = new Color(1, 0.5f, 0);
        }
        else
        {
            healthBar.color = new Color(1, 0, 0);
        }
    }

    /// <summary>
    /// Updates how long the dash bar should be
    /// </summary>
    /// <param name="dashPercent">The percetnage of the dash bar to be fulll (0 - 1.0f)</param>
    public void UpdateDashBar(float dashPercent)
    {
        dashBar.fillAmount = dashPercent;

        //Sets the bar color to cyan when full
        if (dashBar.fillAmount == 1)
        {
            dashBar.color = Color.cyan;
        }
        else
        {
            dashBar.color = Color.gray;
        }
    }

    /// <summary>
    /// Updates the displayer for the portal timer
    /// </summary>
    /// <param name="timeLeft"></param>
    public void UpdatePortalTimer(float timeLeft)
    {
        portalTimer.text = Mathf.RoundToInt(timeLeft).ToString();
    }
}
