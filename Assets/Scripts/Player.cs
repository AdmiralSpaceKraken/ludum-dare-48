using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : Entity
{
    //Singleton
    public static Player instance;

    [SerializeField] private GameObject firePoint;
    [SerializeField] private GameObject arm;
    private int maxHealth = 100;

    [Header("Dashing")]
    [SerializeField] private float timeForDash;             //The length of the dash (in seconds)
    [SerializeField] private float dashSpeedMultiplier;     //The speed boost applied during the dash
    [SerializeField] private float timeForDashCooldown;     //The time it takes for the dash to recharge
    private float dashTimer;
    private float dashCooldownTimer = 0.0f;

    [Header("Default Attack")]
    [SerializeField] private float timeBetweenDefaultAttacks;
    private float defaultAttackTimer;
    [SerializeField] private int defaultAttackDamage;
    [SerializeField] private float defaultAttackMoveSpeed;
    [SerializeField] private GameObject magicMissile;

    [Header("Misc")]
    [SerializeField] private GameObject pauseMenu;

    protected override void Awake()
    {
        base.Awake();
        instance = this;
        ScoreManager.ResetScore();
        Time.timeScale = 1;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }

        //Doesn't do anything when paused
        if(Time.timeScale == 0)
        {
            return;
        }

        Move();
        LookTowardsMouse();
        CheckDefaultAttack();
        //CheckSecondaryAbility();
    }

    /// <summary>
    /// Moves the player based on input and dashes
    /// </summary>
    private void Move()
    {
        //Reduces the dash cooldown
        if (dashCooldownTimer > 0)
        {
            //Updates timer
            dashCooldownTimer -= Time.deltaTime;

            //Pings the UI
            UserInterface.instance.UpdateDashBar(1 - (dashCooldownTimer / timeForDashCooldown));
        }

        //Handles the dashing
        if (dashTimer > 0)
        {
            //Reduces the timer
            dashTimer -= Time.deltaTime;

            //Starts Cooldown
            dashCooldownTimer = timeForDashCooldown;

            //Skips the rest of the method (to lock movement during the dash)
            return;
        }

        //Player Movement
        Vector2 velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        velocity = velocity.normalized;
        velocity *= moveSpeed;
        rb.velocity = velocity;

        //Checks if the player wants to dash (and if they are able to)
        if (Input.GetKeyDown(KeyCode.Space) && dashCooldownTimer <= 0)
        {
            dashTimer = timeForDash;
            rb.velocity *= dashSpeedMultiplier;
        }
    }

    /// <summary>
    /// Rotates the arm to the mouse, and flips the player to look
    /// </summary>
    private void LookTowardsMouse()
    {
        //Moves the arm towards the mouse
        Vector2 vectorToMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float theta = Mathf.Rad2Deg * Mathf.Atan(vectorToMouse.y / Mathf.Abs(vectorToMouse.x));
        arm.transform.rotation = Quaternion.Euler(0, 0, theta * transform.localScale.x);

        //Flips the player based on the direction the mouse is
        if (vectorToMouse.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (vectorToMouse.x < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }


    /// <summary>
    /// Checks whether the player should shoot a magic missile
    /// </summary>
    private void CheckDefaultAttack()
    {
        //Default Attack
        if (defaultAttackTimer <= 0)
        {
            if (Input.GetMouseButton(0))
            {
                //Gets the direction to the mouse
                Vector2 projectileDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - firePoint.transform.position;
                projectileDirection = projectileDirection.normalized;

                //Creates a magic missile
                GameObject newMissile = Instantiate(magicMissile, firePoint.transform.position, Quaternion.identity);
                newMissile.GetComponent<Projectile>().Initialize(Projectile.TargetType.Enemy, projectileDirection, defaultAttackDamage, defaultAttackMoveSpeed);

                //Updates the timer
                defaultAttackTimer = timeBetweenDefaultAttacks;
            }
        }
        else
        {
            defaultAttackTimer -= Time.deltaTime;
        }
    }

    /// <summary>
    /// Modifies the given variable by X multiplier
    /// </summary>
    /// <param name="variableToMod">The variable to modify</param>
    /// <param name="multiplier">How much to multiply the variable by</param>
    public void AddModifier(PactVariable variableToMod, float multiplier)
    {
        switch(variableToMod)
        {
            case PactVariable.damage:
                defaultAttackDamage = Mathf.RoundToInt(defaultAttackDamage * multiplier);
                break;

            case PactVariable.dashCooldown:
                timeForDashCooldown *= multiplier;
                break;

            case PactVariable.dashSpeed:
                dashSpeedMultiplier *= multiplier;
                break;

            case PactVariable.fireRate:
                timeBetweenDefaultAttacks *= multiplier;
                break;

            case PactVariable.maxHealth:
                maxHealth = Mathf.RoundToInt(maxHealth * multiplier);

                //Reduces current health to the new max health
                FullHeal();
                break;

            case PactVariable.moveSpeed:
                moveSpeed *= multiplier;
                break;
        }
    }

    /// <summary>
    /// Deals damage to the player
    /// </summary>
    /// <param name="damageTaken">The amount of damage the player has taken</param>
    public override void TakeDamage(int damageTaken, Quaternion damageDirection)
    {
        base.TakeDamage(damageTaken, damageDirection);

        UserInterface.instance.UpdateHealthBar(health / 100.0f);
    }

    /// <summary>
    /// The player dies
    /// </summary>
    protected override void Die()
    {
        Time.timeScale = 0;
        SceneManager.LoadScene(2);
    }

    /// <summary>
    /// Heals the player up to their maximum
    /// </summary>
    public void FullHeal()
    {
        health = maxHealth;

        UserInterface.instance.UpdateHealthBar(health / 100.0f);
    }

    public void TogglePause()
    {
        if(Time.timeScale == 0.0f)
        {
            Time.timeScale = 1.0f;
            pauseMenu.SetActive(false);
        }
        else
        {
            Time.timeScale = 0.0f;
            pauseMenu.SetActive(true);
        }
    }
}
