using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity
{
    [SerializeField] private int pointsForKill;

    protected void LookTowardsThePlayer()
    {
        //Looks towards the player
        Vector2 vectorToPlayer = Player.instance.transform.position - transform.position;

        //Flips the player based on the direction the mouse is
        if (vectorToPlayer.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (vectorToPlayer.x < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    protected override void Die()
    {
        ScoreManager.AddScore(pointsForKill);
        base.Die();
    }
}
