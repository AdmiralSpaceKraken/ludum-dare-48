using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PactManager : MonoBehaviour
{
    [Header("Main")]
    [SerializeField] private List<Pact> pacts;
    private Pact pact1;
    private Pact pact2;

    [Header("UI")]
    [SerializeField] private GameObject pactMenu;
    [SerializeField] private Text pactTitle1;
    [SerializeField] private Text pactDescription1;
    [SerializeField] private Text pactTitle2;
    [SerializeField] private Text pactDescription2;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            SignPact(pact1);
            Time.timeScale = 1.0f;
            pactMenu.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            SignPact(pact2);
            Time.timeScale = 1.0f;
            pactMenu.SetActive(false);
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1.0f;
            pactMenu.SetActive(false);
        }
    }

    private void SignPact(Pact pactActivated)
    {
        //Applies every modifier to the player
        foreach(PactModifier pactModifier in pactActivated.pactModifiers)
        {
            Player.instance.AddModifier(pactModifier.variable, pactModifier.modifier);
        }
    }

    public void LoadNewPacts()
    {
        Time.timeScale = 0.0f;

        //Generates pact1
        pact1 = pacts[Random.Range(0, pacts.Count)];
        pactTitle1.text = pact1.name;
        pactDescription1.text = pact1.description;

        //Generates pact2
        pact2 = pacts[Random.Range(0, pacts.Count)];
        pactTitle2.text = pact2.name;
        pactDescription2.text = pact2.description;
    }
}
