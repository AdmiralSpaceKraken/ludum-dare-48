using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HollowKnight : Enemy
{
    [SerializeField] private float timeMoving;

    [Header("Combat")]
    [SerializeField] private float timePausedForAttack;
    [SerializeField] private int damage;
    [SerializeField] private int numberOfPellets;
    [SerializeField] private float bulletSpread;
    [SerializeField] private float fireballMoveSpeed;
    [SerializeField] private GameObject fireball;

    private Vector2 movement;

    private void Start()
    {
        StartCoroutine(StartMovement());
    }

    private void Update()
    {
        LookTowardsThePlayer();
        rb.velocity = movement;
    }

    private IEnumerator StartMovement()
    {
        //Chooses a random direction to move
        Vector2 newMovement = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        newMovement = newMovement.normalized * moveSpeed;
        rb.velocity = newMovement;
        movement = rb.velocity;

        yield return new WaitForSeconds(timeMoving + Random.Range(-timeMoving * 0.2f, -timeMoving * 0.2f));

        //Switches to attacking
        StartCoroutine(Attack());
    }

    private IEnumerator Attack()
    {
        //Pauses
        rb.velocity = Vector2.zero;
        movement = rb.velocity;

        //Wait
        yield return new WaitForSeconds(timePausedForAttack / 2);

        //Shoots at the player
        //Gets the direction to the player
        Vector2 projectileDirection = Player.instance.transform.position - transform.position;
        projectileDirection = projectileDirection.normalized;

        //Creates a bunch of fireballs
        float degreeShift = (numberOfPellets / 2) * (Mathf.PI / bulletSpread);
        for (int i = 0; i < numberOfPellets; i++)
        {
            //Rotates the vector to the player by the current degree shift
            Vector2 bulletTarget = projectileDirection;
            Vector2 newTarget = new Vector2(bulletTarget.x * Mathf.Cos(degreeShift) - bulletTarget.y * Mathf.Sin(degreeShift), bulletTarget.x * Mathf.Sin(degreeShift) + bulletTarget.y * Mathf.Cos(degreeShift));
            
            //Creates a new fireballs and launches it towards the new projectile direction
            GameObject newFireball = Instantiate(fireball, transform.position, Quaternion.identity);
            newFireball.GetComponent<Projectile>().Initialize(Projectile.TargetType.Player, newTarget, damage, fireballMoveSpeed);

            //Updates the degree shift
            degreeShift -= (Mathf.PI / bulletSpread);
        }

        //Wait
        yield return new WaitForSeconds(timePausedForAttack / 2);

        //Return to move
        StartCoroutine(StartMovement());
    }
}
