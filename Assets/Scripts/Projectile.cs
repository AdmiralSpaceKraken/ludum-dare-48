using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float deathTimer;

    private TargetType targetType;
    private int damage;
    private Rigidbody2D rb;

    private void Update()
    {
        //Kills the projectile after X time
        deathTimer -= Time.deltaTime;
        if(deathTimer <= 0)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Initializes the projectile
    /// </summary>
    /// <param name="targetType">Sets the type of entity the projectile targets</param>
    /// <param name="direction">The direction the projectile moves</param>
    /// <param name="damage">The damage the projectile deals</param>
    /// <param name="moveSpeed">The speed of the projectile</param>
    public void Initialize(TargetType targetType, Vector2 direction, int damage, float moveSpeed)
    {
        //Sets the bullet rotation to the correct number
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, Vector2.SignedAngle(Vector2.right, direction.normalized));

        //Stores variables
        rb = GetComponent<Rigidbody2D>();
        this.targetType = targetType;
        this.damage = damage;

        //Sets the velocity
        rb.velocity = direction.normalized * moveSpeed;

        //Adds this to the current layer parent
        transform.parent = EnemySpawner.instance.Parent.transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == targetType.ToString())
        {
            //Deals damage then destroys projectile
            collision.GetComponent<Entity>().TakeDamage(damage, Quaternion.Euler(new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z - 90)));
            Destroy(gameObject);
        }
        else if(collision.tag == "Wall")
        {
            Destroy(gameObject);
        }
    }

    public enum TargetType
    {
        Player,
        Enemy
    }
}
