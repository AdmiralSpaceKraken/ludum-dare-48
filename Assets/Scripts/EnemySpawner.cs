using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //Singleton
    public static EnemySpawner instance;

    [Header("Main Spawning Settings")]
    [SerializeField] private float timeBetweenSpawns;
    private float spawnTimer;
    [SerializeField] private List<GameObject> enemiesToSpawn;

    private float TimeBetweenSpawns
    {
        get
        {
            if (currentLayer > 3)
            {
                return timeBetweenSpawns * Mathf.Pow(0.95f, currentLayer);
            }

            return timeBetweenSpawns;
        }
    }

    private int MaxEnemySpawns
    {
        get
        {
            if (currentLayer > 3)
            {
                return 4;
            }

            else return currentLayer;
        }
    }

    [Header("Layer Settings")]
    [SerializeField] private float timeForPortalCharging;
    private float portalTimer;
    private GameObject parent;

    [Header("Misc Settings")]
    [SerializeField] private GameObject pactMenu;

    public GameObject Parent { get { return parent; } }

    private static int currentLayer = 1;
    public static int CurrentLayer { get { return currentLayer; } }

    private void Awake()
    {
        instance = this;
        currentLayer = 1;
    }

    private void Start()
    {
        parent = new GameObject();

        spawnTimer = 0.5f;
        portalTimer = timeForPortalCharging;
    }

    private void Update()
    {
        //Skips this if the game is paused
        if(Time.timeScale == 0)
        {
            return;
        }

        if(portalTimer > 0)
        {
            //Reduces portal timer and updates the UI
            portalTimer -= Time.deltaTime;
            UserInterface.instance.UpdatePortalTimer(portalTimer);

            if (spawnTimer <= 0)
            {
                //Spawns an enemy and resets the cooldown
                SpawnEnemy(Random.Range(1, MaxEnemySpawns + 1));
                spawnTimer = TimeBetweenSpawns + Random.Range(-TimeBetweenSpawns * 0.2f, TimeBetweenSpawns * 0.2f);
            }
            else
            {
                spawnTimer -= Time.deltaTime;
            }
        }
        else
        {
            NextLayer();
        }
    }

    private void SpawnEnemy(int numberOfEnemies)
    {
        for(int i = 0; i < numberOfEnemies; i++)
        {
            //Chooses where to spawn the enemy
            Vector2 newSpawnLocation = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
            newSpawnLocation = newSpawnLocation.normalized * 4;

            //Spawns the enemy
            GameObject newEnemy = Instantiate(enemiesToSpawn[Random.Range(0, (currentLayer > enemiesToSpawn.Count ? enemiesToSpawn.Count : currentLayer))], newSpawnLocation, Quaternion.identity);
            newEnemy.transform.parent = parent.transform;
        }
    }

    private void NextLayer()
    {
        //Deletes all of the current enemies & makes new parent
        Destroy(parent);
        parent = new GameObject();
        spawnTimer = 1.0f;

        //Updates layer and player position
        currentLayer++;
        Player.instance.transform.position = new Vector2(0, 0);

        //Updates the portal timer
        portalTimer = timeForPortalCharging;

        //Resets the player's HP
        Player.instance.FullHeal();

        //Increases the score
        ScoreManager.AddScore(100 * currentLayer);

        //Opens the item menu
        pactMenu.SetActive(true);
        pactMenu.GetComponent<PactManager>().LoadNewPacts();
    }
}
