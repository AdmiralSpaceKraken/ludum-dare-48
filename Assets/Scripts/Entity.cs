using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [Header("Main")]
    [SerializeField] protected float moveSpeed;
    [SerializeField] protected int health;
    [SerializeField] protected GameObject bloodSplatter;

    protected Rigidbody2D rb;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public virtual void TakeDamage(int damageTaken, Quaternion damageDirection)
    {
        //Reduces health
        health -= damageTaken;

        //Spawn blood with damagedirection
        Instantiate(bloodSplatter, transform.position, damageDirection);

        //Checks for death
        if(health <= 0)
        {
            Die();
        }
    }

    protected virtual void Die()
    {
        Destroy(gameObject);
    }
}
