using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenScoreDisplay : MonoBehaviour
{
    [SerializeField] private Text displayText;

    void Start()
    {
        displayText.text = $"Layer: {EnemySpawner.CurrentLayer}\nScore: {ScoreManager.Score}";
    }
}
