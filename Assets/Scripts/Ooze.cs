using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ooze : Enemy
{
    [SerializeField] private int damage;
    [SerializeField] private float attackRange;
    [SerializeField] private float timeForAttacks;
    private float attackTimer;

    private void Update()
    {
        LookTowardsThePlayer();

        //Moves towards the player
        Vector2 vectorToPlayer = (Player.instance.transform.position - transform.position).normalized;
        rb.velocity = vectorToPlayer * moveSpeed;

        //Checks if the player is close enough to deal damage to
        RaycastHit2D rayToPlayer = Physics2D.Raycast(transform.position, vectorToPlayer);
        if(rayToPlayer.transform.tag == "Player" && rayToPlayer.distance < attackRange)
        {
            if (attackTimer <= 0)
            {
                //Deals the damage to the player and resets the cooldown
                Player.instance.TakeDamage(damage, Quaternion.Euler(new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, Vector2.SignedAngle(Vector2.right, vectorToPlayer.normalized))));
                attackTimer = timeForAttacks;
            }
            else
            {
                attackTimer -= Time.deltaTime;
            }
        }
        else
        {
            attackTimer = timeForAttacks;
        }
    }
}
