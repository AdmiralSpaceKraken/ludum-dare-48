using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pact", menuName = "Pact", order = 0)]
public class Pact : ScriptableObject
{
    public List<PactModifier> pactModifiers;
    public string description;
}

[System.Serializable]
public class PactModifier
{
    public PactVariable variable;
    public float modifier;
}

public enum PactVariable
{
    fireRate,
    damage,
    dashSpeed,
    dashCooldown,
    moveSpeed,
    maxHealth
}
